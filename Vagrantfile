# -*- mode: ruby -*-
# vi: set ft=ruby :
# kate: syntax Ruby;


# Configuration
provision_local = "n"
box_ip_address = "192.168.4.66"
box_name = "research"


# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"


Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  config.vm.box = "centos65-32"
  config.vm.hostname = box_name

  config.vm.network :private_network, ip: box_ip_address

  config.vm.provider :virtualbox do |vb|
    vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    vb.customize ["modifyvm", :id, "--memory", "2048"]
  end

  config.vm.synced_folder ".", "/vagrant",
    owner: "root",
    group: "vagrant",
    mount_options: ["dmode=775", "fmode=664"]

  # Provision local environment with Ansible
  if provision_local == "y"
    config.vm.provision "ansible" do |ansible|
      ansible.playbook = "provisioning/local.yml"
      ansible.host_key_checking = false
      ansible.ask_sudo_pass = true
      ansible.extra_vars = {local_ip_address:box_ip_address}
      # Optionally allow verbose output from Ansible
      #ansible.verbose = 'vvvv'
    end
  end

  # Provision Vagrant box with Ansible
  config.vm.provision "ansible" do |ansible|
    ansible.playbook = "provisioning/site.yml"
    ansible.host_key_checking = false
    ansible.extra_vars = {user:"vagrant"}
    #ansible.inventory_path = "provisioning/hosts-vagrant"
    # Optionally allow verbose output from Ansible
    #ansible.verbose = 'vvvv'
  end

end
