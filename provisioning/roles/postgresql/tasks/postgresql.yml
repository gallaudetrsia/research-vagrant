---

- name: Install PostgreSQL packages
  yum: pkg={{ item }} state=installed
  with_items:
    - postgresql
    - postgresql-contrib
    - postgresql-server
    - postgresql-libs
    - postgresql-plpython
    - python-psycopg2
  tags: postgresql
  sudo: true

- name: Initialize PostgreSQL database
  command: service postgresql initdb
      creates=/var/lib/pgsql/data/postgresql.conf
  tags: postgresql
  sudo: true

- name: Start PostgreSQL and enable at boot
  service: name=postgresql
      enabled=yes
      state=started
  tags: postgresql
  sudo: true

- name: Set up local postgresql.conf file
  action: template src=postgresql.conf.j2 dest=/var/lib/pgsql/data/postgresql.conf
      owner=postgres
      group=postgres
      mode=0640
  tags: postgresql
  sudo: true
  notify: Restart PostgreSQL

- name: Set up local pg_hba.conf file
  action: template src=pg_hba.conf.j2 dest=/var/lib/pgsql/data/pg_hba.conf
      owner=postgres
      group=postgres
      mode=0640
  tags: postgresql
  sudo: true
  notify: Restart PostgreSQL

- name: Modify firewall rules to allow network connections to PostgreSQL
  template: src=ferm.conf.j2 dest=/etc/ferm/ferm.d/postgresql.conf
      owner=root
      group=root
      mode=0700
  tags: postgresql
  sudo: true

- name: Create database user
  postgresql_user: name="{{ dbuser }}"
      password="{{ dbpass }}"
      role_attr_flags=CREATEDB,CREATEROLE,CREATEUSER
  tags: postgresql
  sudo: true

- name: Create database
  postgresql_db: name={{ dbname }}
      owner={{ dbuser }}
      encoding='UTF-8'
      lc_collate="en_US.UTF-8"
      lc_ctype="en_US.UTF-8"
      template='template0'
  tags: postgresql
  sudo: true

- name: Set up password file to access live database
  action: template src=live_pgpass.j2 dest=/root/.pgpass
      owner=root
      group=root
      mode=0600
  tags: postgresql
  sudo: true

- name: Dump database from production system
  command: /usr/bin/pg_dump
      -f /tmp/{{ live_dbname }}.sql
      --no-privileges
      --no-owner
      --no-reconnect
      -h {{ live_dbhost }}
      -U {{ live_dbuser }}
      {{ live_dbname }}
  tags: postgresql
  sudo: true

- name: Set up password file to access development database
  action: template src=dev_pgpass.j2 dest=/root/.pgpass
      owner=root
      group=root
      mode=0600
  tags: postgresql
  sudo: true

- name: Import production data into local database
  command: /usr/bin/psql
      -h '{{ dbhost }}'
      -U '{{ dbuser }}'
      -d '{{ dbname }}'
      -f /tmp/{{ live_dbname }}.sql
  tags: postgresql
  sudo: true
